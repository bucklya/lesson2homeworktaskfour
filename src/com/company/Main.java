package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Программа предназначенна для определения слов и предложений палиндромов");
        System.out.println("Введите слово или предложение которое вы хотите проверить:");
        Scanner importText = new Scanner(System.in);
        String textString = importText.nextLine();
        textString = textString.replaceAll("[^a-zA-Zа-яА-Я]","");
        textString = textString.toUpperCase();
        StringBuffer textSB = new StringBuffer(textString);

        StringBuffer textSBReverse = new StringBuffer(textString);
        textSBReverse.reverse();

        String finalTextOne = textSB.toString();
        String finalTextTwo = textSBReverse.toString();

        if (finalTextOne.equals(finalTextTwo)) {
            System.out.println("Этот текст является палиндромом");
        } else {
            System.out.println("Этот текст не является палиндромом");
        }

//        System.out.println(finalTextOne);
//        System.out.println(finalTextTwo);
    }
}
